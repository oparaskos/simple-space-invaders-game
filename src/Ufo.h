#ifndef __UFO_H__
#define __UFO_H__
#include "Entity.h"
const float UFO_START_X = 00.0f;
const float UFO_START_Y = 48.0f;
const int UFO_TEXTURE_COLS = 1;
const int UFO_X_VELOCITY = 45;
const int UFO_COLLISION_RADIUS = 13;
const int UFO_APPERANCE_INTERVAL=34;//seconds
const int SCREEN_WIDTH = (6+11)*(24+5);

class Ufo : public Entity
{
public:
	Ufo ();
	virtual ~Ufo ();
	void update( );
	
	//! gets called on collision
	void alertCollision();
	
	void draw();
	void die();
	
	//! where to render the UFO
	void setRenderWindow(sf::RenderWindow* p_win);
	
	//! restore to defaults
	void restart();
	
	//! reset the score (numUfosKilled)
	void resetScore();
	
	//! how many invaders have been killed?
	unsigned int getNumHits();
	
	//! is this UFO dead?
	bool m_IsDead;
private:
	sf::Texture m_Texture;
	sf::Clock clock; 
	long m_LastFrameTime;
	unsigned int m_NumHits;
};

#endif /* __UFO_H__ */

