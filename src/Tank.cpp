#include "Tank.h"

Tank::Tank(){
	setCollisionLayer(COLLAYER_TANK);
	setSize(sf::Vector2f(52/TANK_NUMFRAMES,16));
	if(!m_Texture.loadFromFile("images/tank.png")){
		printf("Error Loading Image!");
	}
	clock.restart();
	m_PrevKey = sf::Keyboard::Escape;
}

Tank::~Tank(){}

bool Tank::isDead(){return m_IsDead;}
void Tank::ressurect(){m_IsDead = false;}

void Tank::clearCollisionPeers()
{
	#ifdef D_CHECK_COLLISION_PARTNER_INITIALISATION
	fprintf(stderr, "Number Of Collision Partners before clearing: %u\n", m_InvaderPeerList.size());
	#endif
		m_InvaderPeerList.clear();
	#ifdef D_CHECK_COLLISION_PARTNER_INITIALISATION
	if(m_InvaderPeerList.size())
		fprintf(stderr, "Partner Clearing Failed!\n");
	#endif
}

void Tank::addBulletCollisionPeers(std::vector<Entity*> _list)
{
	m_InvaderPeerList.insert(m_InvaderPeerList.end(), _list.begin(), _list.end());
}

void Tank::setRenderWindow(sf::RenderWindow* p_win){ 
	m_Anim.setRenderWindow(p_win);
	m_Anim.init(m_Texture, TANK_NUMFRAMES);
	p_RenderWindow = p_win;
}

bool Tank::update()
{
	if(m_IsDead)
		return false;
	//Update the bullets
	for(int it = 0; it < m_Bullets.size(); ++it) {
		if((m_Bullets[it])->getPosition().y < 0 || !(m_Bullets[it])->isAlive()) {
			delete (m_Bullets[it]);
			(m_Bullets[it]) = nullptr;
			m_Bullets.erase(m_Bullets.begin()+it);
			continue;
		}
		(m_Bullets[it])->update();
	}
	
	//Clear the last key if its been released
	if(!sf::Keyboard::isKeyPressed(m_PrevKey))
		m_PrevKey = sf::Keyboard::Escape;
	
	/*If the space key is pressed, emough time has elapsed, 
		and the key isnt being held down */
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && 
	       (m_RefireClock.getElapsedTime().asSeconds() > TANK_REFIRE_TIMER)
	       && m_PrevKey!= sf::Keyboard::Space)
	{
		m_RefireClock.restart();
		//spawn a bullet
		#ifdef D_CHECK_COLLISION_PARTNER_INITIALISATION
			fprintf(stderr, "Number Of Collision Partners for Bullets: %u\n", m_InvaderPeerList.size());
		#endif
		m_Bullets.push_back(new Bullet(COLLAYER_ALIEN,TANK_BULLET_DIR));
		m_Bullets[m_Bullets.size()-1]->setRenderWindow(p_RenderWindow); 
		m_Bullets[m_Bullets.size()-1]->setPosition(getPosition());  
		m_Bullets[m_Bullets.size()-1]->setPeerList(m_InvaderPeerList); 
		m_PrevKey = sf::Keyboard::Space;
	}
	
	//if an arrow key is pressed
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || 
			sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
		//move tank left if left arrow, right if right arrow.
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
			m_Velocity.x = TANK_SPEED * -1;
		}else{
			m_Velocity.x = TANK_SPEED * 1;
		}
		//update position.
		float targX = m_Position.x + (m_Velocity.x*clock.getElapsedTime().asSeconds());
		if(targX > TANK_BEGIN_BOUNDARY && targX < TANK_END_BOUNDARY){
			setPosition(sf::Vector2f(targX, m_Position.y));
		}
		//restart clock.
		clock.restart();
		return true;
	}
	//restart clock
	clock.restart();
	//Clear the velocity if no arrows are pressed
	m_Velocity.x = 0;
	return true;
}

void Tank::draw()
{
	if(m_IsDead)
		return;
	for(int it = 0; it < m_Bullets.size(); ++it)
	{
		(m_Bullets[it])->draw();
	}
	m_Anim.setPosition(getPosition().x, getPosition().y);
	m_Anim.draw();
	
	#ifdef D_SHOW_COLLISION
		drawCollisionBounds(p_RenderWindow);
	#endif
}

void Tank::alertCollision()
{
	m_IsDead = true;
}













