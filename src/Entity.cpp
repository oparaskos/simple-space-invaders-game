#include "Entity.h"
Entity::Entity(){}
Entity::Entity(sf::Vector2f _position){setPosition(_position);}
Entity::~Entity(){}
void Entity::setPosition( sf::Vector2f _position )
{
	m_Position = _position;
	m_CollisionBounds.setPosition(_position);
}

sf::Vector2f Entity::getPosition()
{ 
	return m_Position;
}

//The Below Function is Virtual and should be overridden (should be pure virtual but i'd rather throw a more meaning ful error if someone makes a stupid mistake.):
void Entity::alertCollision()
{
	fprintf(stderr, "WARN: `%s:%d`: Entity::alertCollision() Was Called,\n\
\tDid you forget a cast?\n", __FILE__, __LINE__);
}


void Entity::setSize(sf::Vector2f _size)
{
	m_Size = _size;
	m_CollisionBounds.setSize(_size);
}

AABB* Entity::getAABB()
{
	return &m_CollisionBounds;
}

void Entity::drawCollisionBounds(sf::RenderWindow* _pWin)
{
	m_CollisionBounds.draw(_pWin);
}

int Entity::getCollisionLayer()
{
	return m_CollisionLayer;
}

void Entity::addPeer(Entity* _peer)
{	
	//printf("1 Element being added to collisionlist\n");
	m_PeerList.push_back(_peer);
}

void Entity::setPeerList(std::vector<Entity*> _peerlist)
{
	//printf("collision list for entity being set outright.., new length of %d\n", 
	//		_peerlist.size());
	m_PeerList = _peerlist;
}

void Entity::addPeerList(std::vector<Entity*> _peerlist)
{
	//printf("%d Elements Being Added to collisionlist\n", 
	//		_peerlist.size());
	m_PeerList.insert(m_PeerList.end(), _peerlist.begin(), _peerlist.end());
}

std::vector<Entity*> Entity::checkCollisions()
{
	std::vector<Entity*> ret;
	
	/* if any peers are colliding then add them to the list */
	for(unsigned int it = 0; it < m_PeerList.size(); ++it){
		if(collidingWith((m_PeerList[it])))
			ret.push_back((m_PeerList[it]));
	}
	
	/* still here because it was an originally an array of indeterminate 
			length, this is	equivelant of '\0' in a string: */
	//if(!ret.size())
	//	ret.push_back(nullptr);

	return ret;
}

void Entity::setCollisionLayer(int _l)
{
	m_CollisionLayer = _l;
}

bool Entity::collidingWith(Entity* _partner)
{
	return m_CollisionBounds.isIntersecting(_partner->getAABB());
}













