#ifndef TANK_H
#define TANK_H
#include <SFML/Graphics.hpp> //needed for sprites, clock etc.
#include "Entity.h"
#include <vector>
#include "Bullet.h"
 
const int TANK_NUMFRAMES = 2;
const int TANK_SPEED = 100;//px/s
const int TANK_COLLISION_RADIUS = 24;
const float TANK_REFIRE_TIMER = 0.15f;
const sf::Vector2f TANK_BULLET_DIR=sf::Vector2f(0, -100);
const int TANK_BEGIN_BOUNDARY = 0 + 15;
const int TANK_END_BOUNDARY = /*width-15-tankwidth:*/(6+11)*(24+5) - 15 - 32;
class Bullet;
class Tank : public Entity
{
public:
	Tank();
	virtual ~Tank();
	//! add things for the bullet to collide with
	void addBulletCollisionPeers(std::vector<Entity*> _listptr);
	//! where to render the tank
	void setRenderWindow(sf::RenderWindow* p_win);
	
	bool update();
	void draw();
	
	//! gets called when collided with:
	void alertCollision();
	
	//! clear things for bullets to collide with
	void clearCollisionPeers();
	
	//! is the tank dead?
	bool isDead();
	
	//! make it alive again
	void ressurect();
private:
	sf::Texture m_Texture;
	sf::Clock clock;
	sf::Clock m_RefireClock;
	sf::Keyboard::Key m_PrevKey;
	sf::Vector2f m_Velocity;
	sf::RenderWindow* p_RenderWindow;
	std::vector<Bullet*> m_Bullets;
	std::vector<Entity*> m_InvaderPeerList;
	bool m_IsDead;
};
#endif
