#ifndef INVADER_H
#define INVADER_H
#include "Entity.h"
#include "Bullet.h"
#include <vector>

//! CONSTANTS:
const int NUMROWS = 5;
const int NUMCOLS = 11;
const int _X_ = 0;
const int _Y_ = 1;
const int INVADER_WIDTH = 24;//px
const int INVADER_SPACING = 6;
const int INVADER_COLLISION_RADIUS = 24/2;
const int INVADER_MOVE_DELAY = 3;
const int INVADER_MOVE_DISTANCE = INVADER_WIDTH+INVADER_SPACING;
const int INVADER_TEXTURE_COLS = 2;
const int INVADER_END_OF_SCREEN = (6+11)*(24+5);//Screen Width
const int INVADER_START_OF_SCREEN = 0;
const int INVADER_BLOCK_START_X = INVADER_MOVE_DISTANCE*(INVADER_SPACING/2);
const int INVADER_BLOCK_START_Y = 76;
const int SCREEN_HEIGHT = 480;
const int INVADER_FIRE_FREQ = 30;
const sf::Vector2f INVADER_BULLET_DIR=sf::Vector2f(0, 100);

class Bullet;

enum GameOverCondition{
	GOC_NONE=-1,
	GOC_WIN=0,
	GOC_LOSE=1
};

class Invader : public Entity
{
public:
	Invader();
	virtual ~Invader();
	
	//! Set the position of the object:
	void setPosition( sf::Vector2f _position );
	
	//! Set the starting position of the object:
	void setStartPosition( sf::Vector2f _start );
	
	//! Set the render window to draw to:
	void setRenderWindow(sf::RenderWindow* p_win);

	//! Update the invader (only updates the animation really)
	void update( );
	
	//! gets called when collided with a bullet.
	void alertCollision();
	
	//! draw this invader
	void draw();
	
	//! kills the invader (wont get drawn anymore)
	void die();
	
	//! reset the object to factory settings (as at the start of the game)
	void restart();

	bool m_IsDead;
	sf::RenderWindow* p_RenderWindow;
	int m_AlienType;
	sf::Vector2f m_StartPosition;
	sf::Texture alienTexture_1;
	sf::Texture alienTexture_2;
	sf::Texture alienTexture_3;
};

class InvaderController
{
public:
	InvaderController();
	
	//! Set the render window to draw to:
	void setRenderWindow(sf::RenderWindow* p_win);
	
	//! Update all of the invader alien things
	bool update();
	
	//! Draw all the invader alien things
	void draw();
	
	/*! \brief Get all of the invaders as a list of Entity pointers 
	 * (to use as collision partners for other objects)
	 */
	std::vector<Entity*> getInvaderPeers();
	
	//! Add things for the aliens to collide with.
	void addCollisionPeerList(std::vector <Entity*>);
	
	//! Clear all the things the aliens should collide with
	void clearCollisionPeers();
	
	int getNumInvadersKilled();
	
	//! reset to state at start of game.
	void restart();
		
	//! find out what happened to end the game:
	GameOverCondition gameOverCondition;
	
private:	
	sf::RenderWindow* p_RenderWindow;
	int m_AlienBoundingBoxStart[2];//the [x][y] of the edgemost m_Invader at the top left
	int m_AlienBoundingBoxEnd[2]; //the [x][y] of the edgemost m_Invader at the bottom right
	int m_InvaderMoveDirection; //1 or -1 to multiply by MoveDistance
	int m_NumInvadersDead;
	
	int m_NumUpdatesDone;
	long m_LastMoveTime;
	sf::Vector2f m_InvaderBlockOffset;
	sf::Clock clock; 
	std::vector< Entity* > m_BulletCollisionPeers;
	std::vector< Bullet* > m_Bullets;
	Invader m_Invaders[NUMCOLS][NUMROWS];
};
#endif
