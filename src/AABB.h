#include <SFML/Graphics.hpp>

class AABB
{
public: 
	AABB(float _x, float _y, float _sx, float _sy);
	AABB(){}
	
	/*! \brief Set The Position of the object.*/
	void setPosition(sf::Vector2f _pos);
	
	/*! \brief Set The Position of the object.
	 *
	 * \param x the x position.
	 * \param y the y position.
	 */
	void setPosition(float x, float y);
	
	/*! \brief Set The Size of the object.*/
	void setSize(sf::Vector2f _size);
	
	/*! \brief Set The Size of the object.
	 *
	 * \param _sx the width.
	 * \param _sy the height.
	 */
	void setSize(float _sx, float _sy);
	
	/*! \brief Check Whether 2 AABB's Are Colliding.
	 *
	 * \param *partner pointer to the AABB to check collision against
	 * \return true if there is a collision, false if not.
	 */
	bool isIntersecting(AABB *partner);
	
	/*! \brief Get the start x value of the box. */
	float getMinX(){return m_Position.x;}
	/*! \brief Get the end x value of the box. */
	float getMaxX(){return m_Position.x + m_Size.x;}
	/*! \brief Get the start y value of the box. */
	float getMinY(){return m_Position.y;}
	/*! \brief Get the end y value of the box. */
	float getMaxY(){return m_Position.y + m_Size.y;}
	
	/*! \brief Draw an outline of the collision.
	 *
	 * \details This is for debugging purposes only, it is not efficient!
	 * \param _pWin the destination RenderWindow.
	 */
	void draw( sf::RenderWindow* _pWin );
	
private:
	sf::Vector2f m_Position;
	sf::Vector2f m_Size;
};

