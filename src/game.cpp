#include "game.h"
#include <fstream>
#include "SFML/Window.hpp"

Game::Game(sf::RenderWindow* app) : win(app) //initialise the pointer to the window
{
	/*set the dimensions for drawing from the present window size 
		- allows resizing easily*/
	width=app->getSize().x;
	height=app->getSize().y;
}

Game::Game() //default constructor - nothing to do
{

}

void Game::restart()
{
	// Reset Game-over flag
	m_GameOver = false;
	
	// Cleanup shields
	for (unsigned int it = 0; it < m_Shields.size(); it ++){
		delete(Shield*)(m_Shields[it]);
		m_Shields[it] = nullptr;
		m_Shields.erase(m_Shields.begin()+it);
	}

	// Reset tank position.
	m_Tank.setPosition(sf::Vector2f(width/2.0f, height-30.0f));

	// Create new shields
	for (int i = 0; i < 3; i++) {
		m_Shields.push_back(new Shield());
		((Shield*)m_Shields[i])->setRenderWindow(win);
		((Shield*)m_Shields[i])->setPosition(sf::Vector2f((i+1) * 
			(width/3.0f), height - 120.0f));
	}

	// Clear partnerLists:
	m_Tank.clearCollisionPeers();
	m_InvaderController.clearCollisionPeers();

	// Add Collision Peers:
	std::vector<Entity*> v;
	v.push_back((Entity*)&m_Tank);
	m_InvaderController.addCollisionPeerList(v);
	m_InvaderController.addCollisionPeerList(m_Shields);
	v.clear();
	v.push_back((Entity*)&m_Ufo);
	m_Tank.addBulletCollisionPeers(v);
	m_Tank.addBulletCollisionPeers(m_Shields);
	m_Tank.addBulletCollisionPeers(m_InvaderController.getInvaderPeers());

	// Restart Clock
	Clock.restart();

	// Reset UFO
	m_LastUFOAppearance = 0;	
	m_Ufo.restart();
	m_Ufo.resetScore();

	// Reset invader
	m_InvaderController.restart();

	// Reset lives
	m_Lives = 3;
}

void Game::setRenderWindow(sf::RenderWindow* app)
{
	win = app;

	/* Set the dimensions for drawing from the 
	 * present window size 
	 * (allows resizing easily)
	 */
	width=app->getSize().x;
	height=app->getSize().y;
}

int getHiScore()
{
	int buf;
	std::ifstream f("data/score");
	if(f.good()){
		f >> buf;
		f.close();
		return buf;
	}
	return 0;
}

std::string generateScoreString(int score, int hiScore)
{
	char buf[40];
	// String formatting function, it is old style C but works
	sprintf_s(buf, "SCORE\tHI-SCORE\n\t%d\t\t%d", score, hiScore);
	return std::string(buf);
}


/*! \brief Update the Score file.
 *
 * \details Updates ./data/score to the new highscore
 */
void updateHiScore(int score)
{
	std::ofstream o("data/score");
	if(o.good()){
		o << score;
		o.close();
	}
}

void Game::init()
{

	if(!t.loadFromFile("images/tank.png")){
		printf("Error Loading Image!");
	}
	for(int i = 0; i < 10; i ++){
		Animation a(win);
		a.init(t, 2);
		m_LivesDisplay[i] = a;
	}
	
	//Set up high score display...
	m_ScoreFont.loadFromFile("images/spinvaders.ttf");
	m_ScoreText.setFont(m_ScoreFont);
	
	m_HiScore=getHiScore();
	m_Score=0;
	m_ScoreString=generateScoreString(m_Score,m_HiScore);
	m_ScoreText.setString(m_ScoreString);
	
	m_ScoreText.setPosition(100,0);	
	m_ScoreText.setCharacterSize(20);
	m_ScoreText.setStyle(sf::Text::Bold);
	m_ScoreText.setColor(sf::Color::White);
	
	//Tell Entities Where to draw:
	m_InvaderController.setRenderWindow(win);
	m_GameOverSpr.setRenderWindow(win);
	m_Tank.setRenderWindow(win);
	m_Ufo.setRenderWindow(win);
	
	//Game Over Sprite (Always there, never changes)
	m_GameOverSpr.setPosition((width/2.0f) - (138/2.0f), 30.0f );
	m_GameOverTex.loadFromFile("images/game-over.png");
	m_GameOverSpr.init(m_GameOverTex, 1);
	
	//Call restart routine
	restart();
	
}
void Game::draw()
{
	//draw backgound colour
	win->clear(sf::Color(24,56,64));
	
	//Draw the extra Tanks.
	if(m_Lives > 0){
		for(unsigned int i = 0; i < m_Lives; ++i) {
			m_LivesDisplay[i].draw();
		}
	}
	
	//draw Game over text if you've lost
	if(m_GameOver)
	{
		//Draw gameover screen.
		m_GameOverSpr.draw();
	}
	m_InvaderController.draw();
	m_Tank.draw();
	m_Ufo.draw();
	for (unsigned int it = 0; it < m_Shields.size(); it ++){
		((Shield*)m_Shields[it])->draw();
	}
	win->draw(m_ScoreText);
}

void Game::getInput()
{
	if(win == nullptr)
		return;
	
	// Escape key : exit, to use close button 
	sf::Event e;
	while(win->pollEvent(e))
		if(e.type == sf::Event::Closed)
			win->close();

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) ){
		win->close();
	}
}

void Game::update()
{
	for(unsigned int i = 0; i < m_Lives && i < 10; ++i) {
		if(m_Lives > 10) m_Lives = 10;
		m_LivesDisplay[i].setPosition(15.0f + i*24.0f, (float)height-15.0f);
		m_LivesDisplay[i].setScale(0.45f);
		m_LivesDisplay[i].draw();
	}
	//dont update if game is lost
	if(m_GameOver)
	{
		//Update gameover screen.
		m_GameOverSpr.update();
		
		if(Clock.getElapsedTime().asSeconds() - m_GameOverStart >
				GAMEOVER_SCREEN_DELAY){
			//If the game over screen has been up for a while restart now
			restart();
		}
		return;
	}
	if(!m_Tank.update()) {
		m_Lives --;
		if(m_Lives < 0){
			m_Ufo.resetScore();
			//Game Over...
			m_GameOver = true;
			m_GameOverStart = Clock.getElapsedTime().asSeconds();
			return;
		}
		//TODO: Some kind of explosion animation / delay
		m_Tank.ressurect();
	}
	if(!m_InvaderController.update())
	{
		if(m_InvaderController.gameOverCondition == GOC_WIN){
			//If Win instant restart
			restart();
			return;
		}
		m_Ufo.resetScore();
		//Game Over...
		m_GameOver = true;
		m_GameOverStart = Clock.getElapsedTime().asSeconds();
	}
	for (unsigned int it = 0; it < m_Shields.size(); it ++){
		((Shield*)m_Shields[it])->update();
	}
	
	if(Clock.getElapsedTime().asSeconds() - m_LastUFOAppearance >
		UFO_APPERANCE_INTERVAL)
	{
		if(m_LastUFOAppearance < 0){
			fprintf(stderr, "m_LastUFOAppearance`%d` not initialised!\n",
				(int)m_LastUFOAppearance);
		}
		//Spawn a UFO gliding across the screen...
		m_Ufo.restart();
		m_LastUFOAppearance = Clock.getElapsedTime().asSeconds();
	}
	
	m_Ufo.update();
	if(m_Score > m_HiScore){
		m_HiScore = m_Score;
		updateHiScore(m_Score);
	}
	m_Score = m_InvaderController.getNumInvadersKilled() * 10 + m_Ufo.getNumHits() * 200;
	m_ScoreString=generateScoreString(m_Score,m_HiScore);
	m_ScoreText.setString(m_ScoreString);
}

void Game::play() // The game loop
{
	// Get input from the user
	getInput();

	// Update game variables 
	//	(let things respond to user input and update their locations)
	update();

	// Draw to the offscreen buffer
	draw();

	// Swap the backbuffer witht the front buffer
	win->display();
}

Game::~Game()
{
	for (unsigned int it = 0; it < m_Shields.size(); it ++){
		delete(Shield*)(m_Shields[it]);
	}
}



