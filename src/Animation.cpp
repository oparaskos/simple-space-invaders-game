#include "Animation.h"
#include <iostream>

/*we need a static clock - it needs to be implemented
in the cpp file as well as declared in the header*/
sf::Clock Animation::clock;



Animation::Animation() //default constructor
{
}
//constructor from a pointer to a window
Animation::Animation(sf::RenderWindow* p_win)
{
	p_w=p_win;
}

void Animation::draw()
{
	//the draw method should draw the animation's sprite
	//	in the window only if the animation is visible
	//use p_w to call draw
	if(visible) p_w->draw(s);
}
//assumes that the texture already exists
void Animation::init(sf::Texture& t, int columns)
{
	//const sf::Texture * t = textureManager.getTexture(strFilepath);
	count = 0;
	started=true;
	visible=true;
	s.setTexture(t);
	cols=columns;
	//work out the imageHeight from the texture information
	//you can get texture information using t.getSize().x etc.
	imageHeight= t.getSize().y;
	imageWidth= t.getSize().x;
	tileWidth= t.getSize().x / columns;

	r.left=0;
	r.top=0;
	r.width=tileWidth;
	r.height=imageHeight;

	//set the sprites texture rectangle to r
	s.setTextureRect(r);
}

void Animation::setPosition(float x, float y){ s.setPosition(x, y); }
void Animation::setDelay(double d){	delay = d; }
//animation will be started unless stopped:
void Animation::start(){ started = true; }
void Animation::stop(){ started = false; }   //stills the animation
void Animation::setVisible(){ visible = true; }
void Animation::setInvisible(){ visible = false; }
void Animation::setScale(float _s){s.setScale(_s, _s);}
void Animation::setScale(float _x, float _y){s.setScale(_x, _y);}
void Animation::advanceFrame(){
	count + 1 < cols ? count ++ : count = 0;
	r.left = tileWidth*count; //move texture rect right
	//set the sprite's rectangle to r.
	s.setTextureRect(r);
}
void Animation::update( )
{

	//you need to get this working if you want animation
	/*you can access the clock time as a double via
			clock.getElapsedTime().asSeconds()*/
	if(clock.getElapsedTime().asSeconds()-lastTime>delay)
	{
		//ready for next run through:
		lastTime=clock.getElapsedTime().asSeconds();
		//move the rectangle r to allow animation

		++count;
		if(count==cols) //WE HAVE MOVED OFF THE RIGHT OF THE IMAGE
		{
			r.left=0;           //reset texture rect at left
					
			count=0;			//reset count
		}
		else
		{
			r.left+=tileWidth; //move texture rect right
		
		}
		
		//set the sprite's rectangle to r.
		s.setTextureRect(r);
	}				

}

