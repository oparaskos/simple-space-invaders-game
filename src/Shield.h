#ifndef SHIELD_H
#define SHIELD_H
#include "Entity.h"
const int SHIELD_TEXTURE_COLS = 5;
class Shield : public Entity
{
public:
	Shield();
	virtual ~Shield();
	void update();
	void draw();
	
	//! gets called on collision
	void alertCollision();
	
	//! where to draw the Shield
	void setRenderWindow(sf::RenderWindow* p_win);
private:
	int m_Health;
	sf::Texture m_Texture;
	bool m_IsAlive;
	sf::RenderWindow* p_RenderWindow;
};

#endif
