#include <SFML/Graphics.hpp>

class AABB
{
public: 
	AABB(float _x, float _y, float _sx, float _sy);
	
	setPosition(sf::Vector2f _pos);
	setPosition(float x, float y);
	
	setSize(sf::Vector2f _size);
	setSize(float _sx, _sy);
	
	bool collidingWith(AABB* partner);
	
	AABB intersect(AABB const & partner);
	static AABB intersect(AABB const & a, AABB const & b);
private:
	sf::Vector2f m_Start;
	sf::Vector2f m_End;
};

