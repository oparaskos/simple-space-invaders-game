#ifndef ANIMATION_H
#define ANIMATION_H
#include <SFML/Graphics.hpp> //needed for sprites, clock etc.

class Animation
{
public:
	Animation(); //a default constructor to comply with good practice - does nothing
	Animation(sf::RenderWindow* p_win); //a constructor from a pointer to the window that the animation draws itself in
	void draw();  //draws the animation to the window
	//void init(sf::Texture& t, int columns); //assumes that the texture already exists - this function then sets up everything else
	void init(sf::Texture& t, int columns); //assumes that the texture already exists - this function then sets up everything else
	void start();  //animation will be started unless stoped - but this can restart
	void stop();   //stills the animation
	void setVisible();  //by default the animation is visible 
	void setInvisible(); // renders the animation invisible
	void update(); //will update against the animation clock
	void setDelay(double d);  //can be used to alter the speed of the animation
	void setPosition(float x, float y);
	void setRenderWindow(sf::RenderWindow* _pWin){p_w = _pWin;}
	void advanceFrame();
	void setScale(float _s);
	void setScale(float _x, float _y);
	float getWidth(){return (float)r.width;}
	float getHeight(){return (float)r.height;}
	sf::Vector2f getHalfSize(){return sf::Vector2f(r.width/2.0f, r.height/2.0f);}

private:
	
	sf::Sprite s; //set to refer to the texture used in init()
	sf::IntRect r; //the part of the sprite active
	sf::RenderWindow* p_w;  //pointer to the window
	int cols;  //how many columnss there are in the sprite tile
	int imageWidth; //the width of the texture
	int imageHeight; //the height of the texture
	int tileWidth; //the width of a tile
	int count;
	double lastTime;
	double delay;  //the animation progresses one tile every delay seconds
	bool visible;  //true if it's visible
	bool started;  // true if it's started
	static sf::Clock clock; // a clock shared by all animations
};
#endif
