#include <SFML/Graphics.hpp>
#include <iostream>
#include "game.h"
#include "Animation.h"

//test the ball game

//24+5 is the invader size accounting for space between invaders
//5+11 is the 11 columns + 6 extra spaces for invaders to move into:
const unsigned int width=(6+11)*(24+5);   //desired width of the starting window
const unsigned int height=480;	 //desired height of the starting window
int main()
{
	
    // Create main window
    sf::RenderWindow App(sf::VideoMode(width, height), "The Simple Ball Game");  
	//create game
	Game g(&App);//g is the simple ball game
	//initialise game - loads resources
	g.init();
	
	// Start the game loop
    while (App.isOpen())
    {	
				 //all of the activity to do with playing
				 //is handled by the game
				 g.play(); 											

    }
    return EXIT_SUCCESS;
}
