#include "AABB.h"

AABB::AABB(float _x, float _y, float _sx, float _sy)
{
	setPosition(_x, _y);
	setSize(_sx, _sy);
}

void AABB::setPosition(sf::Vector2f _pos)
{
	m_Position = _pos;
}

void AABB::setPosition(float _x, float _y)
{
	m_Position.x = _x;
	m_Position.y = _y;
}

void AABB::setSize(sf::Vector2f _size)
{
	m_Size = _size;
}
void AABB::setSize(float _sx, float _sy)
{
	m_Size.x = _sx;
	m_Size.y = _sy;
}

/*! \brief Check .
 *
 * \details <More Detailed description>
 */
bool AABB::isIntersecting(AABB * B)
{
	return  !((getMaxX() < B->getMinX()) || 
		(B->getMaxX() < getMinX()) || 
		(getMaxY() < B->getMinY()) || 
		(B->getMaxY() < getMinY()));
}

void AABB::draw( sf::RenderWindow* _pWin )
{
	sf::ConvexShape boundingBox;
	
	//Create the shape:
	boundingBox.setPointCount(4);
	boundingBox.setPoint(0, sf::Vector2f(m_Position.x, m_Position.y));
	boundingBox.setPoint(1, sf::Vector2f(m_Position.x + m_Size.x, m_Position.y));
	boundingBox.setPoint(2, sf::Vector2f(m_Position.x + m_Size.x, m_Position.y +
									 m_Size.y));
	boundingBox.setPoint(3, sf::Vector2f(m_Position.x, m_Position.y + m_Size.y));
	
	//Set the colour to blue
	boundingBox.setFillColor(sf::Color(0,0,255,128));
	boundingBox.setOutlineColor(sf::Color::Blue);
	boundingBox.setOutlineThickness(1);
	
	//position to zero as the points have position already
	boundingBox.setPosition(0, 0);
	
	//if there is no renderwindow make an error!
	if(_pWin != nullptr){
		_pWin->draw(boundingBox);
		return;
	}
	fprintf(stderr, "Render Window Was NULL! F%s, L%s", __FILE__, __LINE__);
}
