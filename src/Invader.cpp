#include "Invader.h"
#include <random>
#include <vector>

Invader::Invader()
{
	setSize(sf::Vector2f(22,17));
	restart();
}

Invader::~Invader(){}

void Invader::restart()
{
	setCollisionLayer(COLLAYER_ALIEN);
	m_IsDead = false;
}

void Invader::setPosition( sf::Vector2f _position ) 
{
	Entity::setPosition(sf::Vector2f(m_StartPosition.x + _position.x, 
				m_StartPosition.y + _position.y));
}
void Invader::setRenderWindow(sf::RenderWindow* p_win)
{
	p_RenderWindow = p_win;
	//TODO: Not load 3 files 11*5 times 
	if(!alienTexture_1.loadFromFile("images/aliens_basic_3.png"))
	{
		fprintf(stderr, "Couldnt Load File! aliens_basic_3.png\n");
	}
	if(!alienTexture_2.loadFromFile("images/aliens_basic_2.png"))
	{
		fprintf(stderr, "Couldnt Load File! aliens_basic_2.png\n");
	}
	if(!alienTexture_3.loadFromFile("images/aliens_basic.png"))
	{
		fprintf(stderr, "Couldnt Load File! aliens_basic.png\n");
	}
	m_Anim.setRenderWindow(p_win); 
	switch(m_AlienType){
	case 1:
		m_Anim.init(alienTexture_1, INVADER_TEXTURE_COLS);
		break;
	case 2:
		m_Anim.init(alienTexture_2, INVADER_TEXTURE_COLS);
		break;
	case 3:
	default:
		m_Anim.init(alienTexture_3, INVADER_TEXTURE_COLS);
	}
}

void Invader::update( )
{
	//dont bother with dead aliens:
	if(m_IsDead)
		return;

	/*We only advance the frame when the 
		alien moves in space invaders therefore 
		updating the anim is overkill*/
	m_Anim.advanceFrame();
}
void Invader::setStartPosition( sf::Vector2f _start)
{
	m_StartPosition = _start;
}
void Invader::alertCollision(){die();}
void Invader::draw()
{
	if(m_IsDead)
		return;
	m_Anim.setPosition(m_Position.x, m_Position.y);
	m_Anim.draw();
	#ifdef D_SHOW_COLLISION
		drawCollisionBounds(p_RenderWindow);
	#endif
}
void Invader::die()
{
	m_IsDead = true;
	setCollisionLayer(-1);
}

void InvaderController::clearCollisionPeers()
{
#ifdef D_CHECK_COLLISION_PARTNER_INITIALISATION
		fprintf(stderr, "Number Of Collision Partners before clearing: %u (INVADER)\n", m_BulletCollisionPeers.size());
#endif
	m_BulletCollisionPeers.clear();
#ifdef D_CHECK_COLLISION_PARTNER_INITIALISATION
if(m_BulletCollisionPeers.size())
	fprintf(stderr, "Partner Clearing Failed!\n");
#endif
}

InvaderController::InvaderController()
{
	restart();
}

void InvaderController::setRenderWindow(sf::RenderWindow* p_win){ 
	p_RenderWindow = p_win;
	for(int x = 0; x < NUMCOLS; x ++){ 
		for(int y = 0; y < NUMROWS; y ++){
			m_Invaders[x][y].setRenderWindow(p_win);
		}	
	}
}

void InvaderController::restart()
{
	m_NumInvadersDead = 0;
	m_NumUpdatesDone = 0;
	//Cleanup Bullets
	for(int it = 0; it < m_Bullets.size(); it++)
	{
		delete m_Bullets[it];
	}
	m_Bullets.clear();

	//Restart invaders
	for(int x = 0; x < NUMCOLS; ++x){
		for(int y = 0; y < NUMROWS; ++y){
			m_Invaders[x][y].restart();
			sf::Vector2f a(m_InvaderBlockOffset.x, m_InvaderBlockOffset.y);
			m_Invaders[x][y].setPosition(a);
		}
	}
	
	srand(245254); //TODO: Use time()
	m_InvaderMoveDirection = 1;
	m_InvaderBlockOffset.x = INVADER_BLOCK_START_X;
	m_InvaderBlockOffset.y = INVADER_BLOCK_START_Y;
	m_AlienBoundingBoxStart[0] = 0;
	m_AlienBoundingBoxStart[1] = 0;
	m_AlienBoundingBoxEnd[0] = NUMCOLS;
	m_AlienBoundingBoxEnd[1]  = NUMROWS;
	
	for(int x = 0; x < NUMCOLS; ++x){
		for(int y = 0; y < NUMROWS; ++y){
			if(y == 0){ //first row aliens are tiny
				m_Invaders[x][y].m_AlienType=1;
			}else if(y == 1 || y==2){//next 2 are medium
				m_Invaders[x][y].m_AlienType=2;
			}else{
				m_Invaders[x][y].m_AlienType=3;
			}

			sf::Vector2f a(m_InvaderBlockOffset.x, m_InvaderBlockOffset.y);
			m_Invaders[x][y].setStartPosition(sf::Vector2f(x*(INVADER_MOVE_DISTANCE), 
								y*(INVADER_MOVE_DISTANCE)));
			m_Invaders[x][y].setPosition(a);
		}
	}
	
	clock.restart();
	m_LastMoveTime = clock.getElapsedTime().asSeconds();
	update();
	gameOverCondition = GOC_NONE;
}

int InvaderController::getNumInvadersKilled()
{
	//Count how many dead invaders:
	m_NumInvadersDead = 0;
	for(int x = 0; x < NUMCOLS; ++x)
		for(int y = 0; y < NUMROWS; ++y)
			if(m_Invaders[x][y].m_IsDead) m_NumInvadersDead ++;
	return m_NumInvadersDead;
}

bool InvaderController::update()
{
	//update bullets.
	for(int i = 0; i < m_Bullets.size(); i++)
	{
		if(m_Bullets[i] == nullptr)
			continue;
		if(m_Bullets[i]->getPosition().y > 480 || !m_Bullets[i]->isAlive()){
			delete m_Bullets[i];
			m_Bullets[i] = nullptr;
  			m_Bullets.erase(m_Bullets.begin()+i);
			continue;
		}
		m_Bullets[i]->update();
	}
	
	//Work out if the aliens need to move this frame?
	float x = clock.getElapsedTime().asSeconds();
	/* if enough time hasnt yet passed...*/
	if(x < (INVADER_MOVE_DELAY - (getNumInvadersKilled()/20))){
		return true;
	}
	m_NumUpdatesDone ++;
	clock.restart();
	m_LastMoveTime = clock.getElapsedTime().asSeconds();
	
	if(rand()%100  < INVADER_FIRE_FREQ){
		//Work out y position;
		sf::Vector2f _bulletStart;
		_bulletStart.y = m_InvaderBlockOffset.y;
		_bulletStart.y += (m_AlienBoundingBoxEnd[_Y_] * INVADER_MOVE_DISTANCE);
		_bulletStart.x = rand()%(INVADER_MOVE_DISTANCE*m_AlienBoundingBoxEnd[_X_]) + m_InvaderBlockOffset.x;
		
		m_Bullets.push_back(new Bullet(COLLAYER_TANK, INVADER_BULLET_DIR));
		m_Bullets[m_Bullets.size()-1]->setRenderWindow(p_RenderWindow); 
		m_Bullets[m_Bullets.size()-1]->setPosition(_bulletStart);
#ifdef D_CHECK_COLLISION_PARTNER_INITIALISATION
		fprintf(stderr, "Number Of Collision Partners for Bullet: %u\n", m_BulletCollisionPeers.size());
#endif
		m_Bullets[m_Bullets.size()-1]->setPeerList(m_BulletCollisionPeers); 
	}


	bool _scanSuccess = true;
	/*Scans from outside of boundary (most likely aliens to be 
		destroyed in a way that effects the bounding box 
		are going to be around the edges)*/ 

	//Scan for start X (left of bounding box)
	m_AlienBoundingBoxEnd[_X_] = NUMCOLS;
	for(int x = NUMCOLS - 1; x >= 0; --x){
		_scanSuccess = true;
		for(int y = 0; y < NUMROWS; ++y){
			if(!m_Invaders[x][y].m_IsDead){
				_scanSuccess = false;
				break;
			}
		}
		if(!_scanSuccess)
			break;
		m_AlienBoundingBoxEnd[_X_] = x;	
	}
	//Scan for end X (right of bounding box)
	m_AlienBoundingBoxStart[_X_] = 0;
	for(int x = 0; x < NUMCOLS; ++x){
		_scanSuccess = true;
		for(int y = 0; y < NUMROWS; ++y){
			if(!m_Invaders[x][y].m_IsDead){
				_scanSuccess = false;
				break;
			}
		}
		if(!_scanSuccess)
			break;
		m_AlienBoundingBoxStart[_X_] = x;
	}
	//Scan for end Y (bottom of bounding box)
	m_AlienBoundingBoxEnd[_Y_] = NUMROWS;
	for(int y = NUMROWS - 1; y >= 0; --y){
		_scanSuccess = true;
		for(int x = m_AlienBoundingBoxStart[_X_]; x <= m_AlienBoundingBoxEnd[_X_]; ++x){
			if(!m_Invaders[x][y].m_IsDead){
				_scanSuccess = false;
				break;
			}
		}
		if(!_scanSuccess)
			break;
		m_AlienBoundingBoxEnd[_Y_] = y;
	}
	m_AlienBoundingBoxStart[_Y_] = 0;
	
	/*if all the invaders are dead, this is faster than scanning again 
		as we already did a bounds check.*/
	if(m_AlienBoundingBoxEnd[_Y_] - m_AlienBoundingBoxStart[_Y_] == 0){
		gameOverCondition = GOC_WIN;
		return false;
	}
	
	
	
	#ifndef D_STOP_ALIENS_MOVING
		//position.x += MoveDistance
		m_InvaderBlockOffset.x += INVADER_MOVE_DISTANCE * m_InvaderMoveDirection;
		//work out where the end of the invader 'block' is:
		sf::Vector2f _end, _start;
		
		_start.x = m_AlienBoundingBoxStart[_X_] * INVADER_MOVE_DISTANCE;
		_start.y = m_AlienBoundingBoxStart[_Y_] * INVADER_MOVE_DISTANCE;
		
		_end.x = m_AlienBoundingBoxEnd[_X_] * INVADER_MOVE_DISTANCE;
		_end.y = m_AlienBoundingBoxEnd[_Y_] * INVADER_MOVE_DISTANCE;
		
		
		#ifndef D_STOP_ALIENS_MOVING
			_start.x += m_InvaderBlockOffset.x;
			_end.x += m_InvaderBlockOffset.x;
			_start.y += m_InvaderBlockOffset.y;
			_end.y += m_InvaderBlockOffset.y;
		#endif
		
		//if end of block goes off the edge of the screen
		if(_end.x > INVADER_END_OF_SCREEN ||
			_start.x < INVADER_START_OF_SCREEN){
			//undo last step
			m_InvaderBlockOffset.x += INVADER_MOVE_DISTANCE * m_InvaderMoveDirection * -1;
			//position.y += MoveDistance
			m_InvaderBlockOffset.y += INVADER_MOVE_DISTANCE;
			m_InvaderMoveDirection *= -1;
		}

		//If the bottom Row gets to end position end the game...
		//TODO: Game End...
		//gameOverCondition = GOC_LOSE;
			
		/*Update all the aliens that are alive and well 
			(and a few that aren't, but not worth hunting down):*/
		for(int x = m_AlienBoundingBoxStart[_X_];
				x < m_AlienBoundingBoxEnd[_X_];
				x ++){
			for(int y = m_AlienBoundingBoxStart[_Y_];
					y < m_AlienBoundingBoxEnd[_Y_];
					y ++){
				m_Invaders[x][y].setPosition(m_InvaderBlockOffset);
				m_Invaders[x][y].update();
			}	
		}
		
	#endif
	return true;
}
void InvaderController::draw()
{

	for(int it = 0; it < m_Bullets.size(); ++it)
	{
		m_Bullets[it]->draw();
	}
	for(int x = m_AlienBoundingBoxStart[_X_];
			x < m_AlienBoundingBoxEnd[_X_];
			x ++){
		for(int y = m_AlienBoundingBoxStart[_Y_];
				y < m_AlienBoundingBoxEnd[_Y_];
				y ++){
			m_Invaders[x][y].draw();
		}	
	}
	/*
	 * The below code between #ifdef's is purely for debugging purposes 
	 * and would not be compield into a release version.
	 */
	#ifdef D_SHOW_DEBUG_VISUALISATION
		sf::Vector2f _start;
		sf::Vector2f _end;
		
		_start.x = m_AlienBoundingBoxStart[_X_] * INVADER_MOVE_DISTANCE;
		_start.y = m_AlienBoundingBoxStart[_Y_] * INVADER_MOVE_DISTANCE;
		
		_end.x = m_AlienBoundingBoxEnd[_X_] * INVADER_MOVE_DISTANCE;
		_end.y = m_AlienBoundingBoxEnd[_Y_] * INVADER_MOVE_DISTANCE;
		
		
		#ifndef D_STOP_ALIENS_MOVING
		_start.x += m_InvaderBlockOffset.x;
		_end.x += m_InvaderBlockOffset.x;
		_start.y += m_InvaderBlockOffset.y;
		_end.y += m_InvaderBlockOffset.y;
		#endif
		//Draw a rectangle over the aliens that are still alive.
		sf::ConvexShape polygon;
		 polygon.setPointCount(4);
		 polygon.setPoint(0, sf::Vector2f(_start.x, _start.y));
		 polygon.setPoint(1, sf::Vector2f(_end.x, _start.y));
		 polygon.setPoint(2, sf::Vector2f(_end.x, _end.y));
		 polygon.setPoint(3, sf::Vector2f(_start.x, _end.y));
		 polygon.setFillColor(sf::Color(255,0,0,128));
		 polygon.setOutlineColor(sf::Color::Red);
		 polygon.setOutlineThickness(1);
		 polygon.setPosition(0, 0);
		p_RenderWindow->draw(polygon);
	#endif
}
std::vector<Entity*> InvaderController::getInvaderPeers()
{
	std::vector<Entity*> ret;
	ret.reserve(NUMCOLS*NUMROWS);
	for(int x = 0; x < NUMCOLS; ++x) {
		for(int y = 0; y < NUMROWS; ++y) {
			ret.push_back((Entity*)&m_Invaders[x][y]);
		}
	}
	return ret;
}
void InvaderController::addCollisionPeerList(std::vector<Entity*> _list)
{
	m_BulletCollisionPeers.insert(m_BulletCollisionPeers.end(), 
						_list.begin(), _list.end());
}
