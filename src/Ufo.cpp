#include "Ufo.h"
Ufo::Ufo ()
{
	setCollisionLayer(COLLAYER_UFO);
	setSize(sf::Vector2f(48,21));
	restart();
	resetScore();
}

void Ufo::restart()
{
	setPosition(sf::Vector2f(UFO_START_X, UFO_START_Y));
	m_IsDead = false;
	
	clock.restart();
}

void Ufo::resetScore()
{
	m_NumHits = 0;
}

Ufo::~Ufo()
{
	//TODO: Clean up any ptrs.
}

void Ufo::update()
{
	//X position += velocity*time:
	float newX = clock.getElapsedTime().asSeconds()*UFO_X_VELOCITY;
	setPosition(sf::Vector2f(newX, UFO_START_Y));
}

void Ufo::alertCollision()
{
	die();
	m_NumHits ++;
}

void Ufo::draw()
{
	m_Anim.setPosition(m_Position.x, m_Position.y);
	if(!m_IsDead)
		m_Anim.draw();
}

unsigned int Ufo::getNumHits()
{
	return m_NumHits;
}

void Ufo::die()
{
	m_IsDead = true;
}

void Ufo::setRenderWindow(sf::RenderWindow* p_win)
{
	/*TODO: Not load the file every time a UFO appears 
			(its already going to be in memory!*/
	if(!m_Texture.loadFromFile("images/ufo.png"))
	{
		fprintf(stderr, "Couldnt Load File! `ufo.png`\n");
	}
	m_Anim.setRenderWindow(p_win); 
	m_Anim.init(m_Texture, UFO_TEXTURE_COLS);
}

