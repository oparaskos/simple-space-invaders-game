#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>

#include "Invader.h"
#include "Animation.h"
#include "Tank.h"
#include "Shield.h"
#include "Ufo.h"

//the game class holds all the info and functions for a game using a ball

const float FRAMERATE=30.0f;	//the number of updates per second
const int GAMEOVER_SCREEN_DELAY = 5;
using std::vector;

class Game
{
public:
	
	Game(sf::RenderWindow*);//the game is built (constructed) from a pointer to a window - this pointer is copied so the game can daw to the window
	Game();
	~Game();//destructor
	void setRenderWindow(sf::RenderWindow*);
	void init();//access resources such as bit maps etc.
	void draw();// draws the game picture
	void getInput();//gets the input from the keyboard
	void update();//transfers user input to the model and takes care of any auto update stuff
	void play();//the game loop
	void restart();//restart the game
	void setUpCollisionPartners(); //set up all the collisions for all the objects
	
	
protected:
	//all games will need these few things
	sf::RenderWindow* win;//a pointer to the window to draw to
	unsigned int width;//the original window size - width
	unsigned int height;//the original window size - height
	sf::Clock Clock;//for timing the movement - not always needed
	
	
private:
	//things that are particular to my game
	InvaderController m_InvaderController;
	Tank m_Tank;
	
	std::vector< Entity* > m_Shields;
	Animation m_LivesDisplay[10];
	
	Ufo m_Ufo;
	double m_LastUFOAppearance;
	bool m_GameOver;
	Animation m_GameOverSpr;
	sf::Texture m_GameOverTex;
	double m_GameOverStart;
	
	sf::Font m_ScoreFont;
	sf::Text m_ScoreText;
	std::string m_ScoreString;
	
	unsigned int m_HiScore;
	unsigned int m_Score;
	unsigned int m_Lives;
	
	sf::Texture t;
};
#endif
