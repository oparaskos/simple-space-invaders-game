#ifndef BULLET_H
#define BULLET_H

#include "Entity.h"
#include "Invader.h"
#include "Shield.h"
#include "Tank.h"
#include "Ufo.h"

const int BULLET_COLLISION_RADIUS = 10;
const int BULLET_NUMFRAMES = 1;

//Forward declaration because of circular dependancies:
class Tank;
class Invader;

/*! \class Bullet
 *
 * \details A generic projectile entity can travel in any direction 
 * and target any collision layer as long as it has definitions of what to do
 * for each specific Entity Derivitave.
 */
class Bullet : public Entity
{
public:
	Bullet(int _targetCollisionLayer, sf::Vector2f _velocity);
	virtual ~Bullet();
	
	//! set the RenderWindow to draw the bullet onto:
	void setRenderWindow(sf::RenderWindow* p_win);
	
	 //! update the bullet
	void update(); 
	
	 //! draw the bullet
	void draw(); 
	
	 //! Check the bullet is still alive and well
	bool isAlive();
private:
	//! The Bullet texture, ought to be static really
	sf::Texture m_Texture;

	//! speed and direction of the bullet
	sf::Vector2f m_Velocity;
		
	sf::Clock clock;
	
	//! The target of the bullet
	int m_TargetCollisionLayer;
	
	//! IS the Bullet Alive
	bool m_IsAlive;
		
	//! Where to render the bullet
	sf::RenderWindow* p_RenderWindow;
};
#endif //BULLET_H
