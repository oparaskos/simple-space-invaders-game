#include "Bullet.h"
#include <cassert>
Bullet::Bullet(int _targetCollisionLayer, sf::Vector2f _velocity)
{	
	setCollisionLayer(COLLAYER_BULLET);
	setSize(sf::Vector2f(6,12));
	if(!m_Texture.loadFromFile("images/bullet.png")){
		fprintf(stderr, "Error Loading Image!");
	}
	clock.restart();
	m_TargetCollisionLayer = _targetCollisionLayer;
	m_Velocity = _velocity;
	m_IsAlive = true;
}
Bullet::~Bullet(){}

void Bullet::setRenderWindow(sf::RenderWindow* p_win){ 
	m_Anim.setRenderWindow(p_win);
	m_Anim.init(m_Texture, BULLET_NUMFRAMES);
	p_RenderWindow = p_win;
}
void Bullet::update()
{
#ifdef D_CHECK_COLLISION_PARTNER_INITIALISATION
	if(m_PeerList.size() == 0)
		fprintf(stderr, "No Collision Peers For This Bullet!");
#endif
	//m_Anim.update();
	std::vector<Entity*> collidingWith = checkCollisions();
	sf::Vector2f _newPos = m_Position + (m_Velocity*clock.getElapsedTime().asSeconds());
	setPosition(_newPos);
	clock.restart();
	for(unsigned int it = 0; it < collidingWith.size(); ++it)
	{
		if(collidingWith[it] == nullptr){
			fprintf(stderr, "Collision partner is nullptr\n");
			continue;
		}
		//printf("Collision; Layer %d\n", (*it)->getCollisionLayer());
		if(collidingWith[it]->getCollisionLayer() == m_TargetCollisionLayer){
			switch(m_TargetCollisionLayer){
			case  COLLAYER_ALIEN:
				((Invader*)collidingWith[it])->alertCollision();
				m_IsAlive = false;
				break;
			case COLLAYER_TANK:
				((Tank*)collidingWith[it])->alertCollision();
				m_IsAlive = false;
				break;
			default:
				fprintf(stderr, "Collision Partners\
					 Layer is not accounted for!\n");
				(collidingWith[it])->alertCollision();
			}
		}
		switch((collidingWith[it])->getCollisionLayer()){
		case COLLAYER_SHIELD:
			((Shield*)collidingWith[it])->alertCollision();
			m_IsAlive = false;
			break;
		case COLLAYER_UFO:
			((Ufo*)collidingWith[it])->alertCollision();
			m_IsAlive = false;
			break;
		}
	}
}
void Bullet::draw()
{
	m_Anim.setPosition(getPosition().x, getPosition().y);
	m_Anim.draw();
	#ifdef D_SHOW_COLLISION
		drawCollisionBounds(p_RenderWindow);
	#endif
}
bool Bullet::isAlive()
{
	return m_IsAlive;
}
