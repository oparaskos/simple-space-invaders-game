#ifndef ENTITY_H
#define ENTITY_H
#include "Animation.h"
#include <SFML/Graphics.hpp> //needed for sprites, clock etc.
#include <cmath>
#include "AABB.h"

/*  Collision Layers: */
const int COLLAYER_BULLET = 0;
const int COLLAYER_TANK = 1;
const int COLLAYER_ALIEN = 2;
const int COLLAYER_SHIELD = 3;
const int COLLAYER_UFO = 4;

/*! \class Entity Entity.h "AnimationClass/Entity.h"
 *
 * \details A Base-Class to any object with Collision and something drawable
 * Animated, or not. Handles most of the collision stuff as well.
 */
class Entity
{	
public:
	Entity();
	Entity(sf::Vector2f _position);
	virtual ~Entity();
	/*! \brief Set The Position Of The Entity. */
	void setPosition( sf::Vector2f _position );
	
	/*! \brief Called When a collision has happened.
	 *
	 * \details This function gets called when a collision has occured,
	 * Because collidable peers are stored as pointers it is up to the
	 * Collidable to determine which alertCollision() to call by casting
	 * \example `(Invader*)_partner->alertCollision()` rather than 
	 * `_partner->alertCollision()`
	 */
	virtual void alertCollision();
	
	/*! \brief Returns the Position as a sfml Vector2f */
	sf::Vector2f getPosition() ;

	/*! \brief Add an Entity to check against for collisions.
	 *
	 * \details Adds an entity to this entities Peer List such that it is 
	 * checked for collisions;
	 * \note WARNING: any objects that will be deleted may throw an error.
	 */
	void addPeer(Entity* _peer);
	
	/*! \brief Sets the peer list outright.
	 *
	 * \details Useful if you already have a vector of pointers
	 * (or have a master object to some collection like InvaderController)
	 * \note WARNING: will override existing elements in the list.
	 */
	void setPeerList(std::vector<Entity*> _peerlist);
	
	/*! \brief Adds a vector to the exisitng peer list. */
	void addPeerList(std::vector<Entity*> _peerlist);
	
	/*! \brief Returns the collision radius
	 * 
	 * \details May be useful if at some point i use this class for path 
	 * generation to know the characters width (can the character fit on 
	 * path x), also for non-zero paths (how close can i put my character 
	 * against the wall?)
	 */
	AABB* getAABB();
	
	/*! \brief Draws a circle representing the collision boundary.
	 *
	 * \param _pWin pointer to the render window.
	 */
	void drawCollisionBounds(sf::RenderWindow* _pWin);
	
	/*! \brief Returns the "Collision Layer" of this object.
	 *
	 * \details A Collision Layer is a simple way to see how 2 objects 
	 * should handle the collisions, tells the programmer whether to put 
	 * \example `(Invader*)_partner->alertCollision()` or
	 * `(Shield* )_partner->alertCollision()`
	 */
	int getCollisionLayer();
	
protected:

	/*! \brief Sets the "Collision Layer" of this object.
	 *
	 * \sa Entity::getCollisionLayer();
	 */
	void setCollisionLayer(int _l);
	
	void setSize(sf::Vector2f _size);
	
	/*! \brief Returns True if this object is colliding with _partner
	 */
	bool collidingWith(Entity* _partner);
	
	/*! \brief Returns a vector of objects colliding with this one. */
	std::vector<Entity*> checkCollisions();

	//! See top of this file for a list of Collision Layers or add new ones.
	int 				m_CollisionLayer;
	sf::Vector2f			m_Size;
	//! The Position of this object
	sf::Vector2f 			m_Position;
	//! The Animation of this object
	Animation 			m_Anim;
	//! The vector of objects to check against for collisions.
	std::vector< Entity* >		m_PeerList;
	
	AABB				m_CollisionBounds;
};
#endif
