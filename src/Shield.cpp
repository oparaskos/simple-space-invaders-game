#include "Shield.h"

Shield::Shield()
{
	m_Health = SHIELD_TEXTURE_COLS;
	setCollisionLayer(COLLAYER_SHIELD);
	setSize(sf::Vector2f(45, 32));
	m_IsAlive = true;
}

Shield::~Shield(){}

void Shield::setRenderWindow(sf::RenderWindow* p_win)
{
	p_RenderWindow=p_win;
	//TODO: Not load 3 files 11*5 times 
	if(!m_Texture.loadFromFile("images/barriers.png"))
	{
		printf("Couldnt Load File! 1");
	}
	m_Anim.setRenderWindow(p_win); 
	m_Anim.init(m_Texture, SHIELD_TEXTURE_COLS);
}

void Shield::update()
{
}

void Shield::alertCollision()
{
	m_Health --;
	m_Anim.advanceFrame();
	if(m_Health <= 0){
		m_IsAlive = false;
		setCollisionLayer(-1);
	}
}

void Shield::draw()
{
	if(!m_IsAlive)
		return;
	m_Anim.setPosition(getPosition().x, getPosition().y);
	m_Anim.draw();
	
	#ifdef D_SHOW_COLLISION
		drawCollisionBounds(p_RenderWindow);
	#endif
}	
